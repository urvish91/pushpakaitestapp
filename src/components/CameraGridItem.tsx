/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable jsx-a11y/img-redundant-alt */
import { Container, Grid, IconButton, Typography } from "@material-ui/core";
import MicIcon from "@material-ui/icons/Mic";
import FullScreenIcon from "@material-ui/icons/Fullscreen";
import PlayCircleOutlineIcon from "@material-ui/icons/PlayCircleOutline";

export type CameraGridProps = {
  imageURL: string;
  title: string;
  onClick: (action: string) => void;
};

const CameraGridItem = (props: CameraGridProps) => {
  return (
    <Container style={{ flex: 1, padding: 10 }}>
      <Grid
        container
        justify="space-between"
        alignItems="flex-end"
        style={{
          height: 300,
          borderRadius: 10,
          backgroundImage: `url(${props.imageURL})`,
        }}
      >
        <IconButton aria-label="mic">
          <MicIcon
            fontSize={"large"}
            style={{ color: "white" }}
            onClick={() => props.onClick("mic")}
          />
        </IconButton>

        <IconButton aria-label="play" style={{ alignSelf: "center" }}>
          <PlayCircleOutlineIcon
            fontSize={"large"}
            style={{ color: "white" }}
            onClick={() => props.onClick("play")}
          />
        </IconButton>

        <IconButton aria-label="full_screen">
          <FullScreenIcon
            fontSize={"large"}
            style={{ color: "white" }}
            onClick={() => props.onClick("full_screen")}
          />
        </IconButton>
      </Grid>
      <Typography>{props.title}</Typography>
    </Container>
  );
};

export default CameraGridItem;
