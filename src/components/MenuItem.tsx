import { ButtonBase, Grid, Typography } from "@material-ui/core";
import React from "react";

import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";

type MenuItemProps = {
  icon: React.ReactNode;
  name: string;
  onPress: () => void;
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flex: 1,
      marginTop: 15,
      marginBottom: 15,
    },
  })
);

const MenuItem = (props: MenuItemProps) => {
  const classes = useStyles();

  return (
    <ButtonBase className={classes.root}>
      <Grid
        item
        onClick={props.onPress}
        direction="column"
        justify="center"
      >
        {props.icon}
        <Typography variant="subtitle1">{props.name}</Typography>
      </Grid>
    </ButtonBase>
  );
};

export default MenuItem;
