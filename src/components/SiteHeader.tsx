import React from "react";
import { Grid, Avatar, Typography, AppBar } from "@material-ui/core";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import { deepPurple } from "@material-ui/core/colors";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flex: 1,
      padding: 8,
    },
    large: {
      width: theme.spacing(7),
      height: theme.spacing(7),
      backgroundColor: deepPurple[500],
      color: theme.palette.getContrastText(deepPurple[500]),
      fontSize: 24,
    },

    divider: {
      margin: theme.spacing(2, 0),
      backgroundColor: "red",
    },

    offset: theme.mixins.toolbar,
  })
);

const SiteHeader = () => {
  const classes = useStyles();

  return (
    <React.Fragment>
      <AppBar position="fixed" color="primary" className={classes.root}>
        <Grid container item justify="space-between">
          <Grid container item xs={9} alignItems="center">
            <Avatar className={classes.large}>H</Avatar>
            <Typography variant="h4" style={{ marginLeft: 10 }}>
              Pushpak.ai
            </Typography>
          </Grid>
          <Grid item justify="flex-end">
            <Avatar className={classes.large}>H</Avatar>
          </Grid>
        </Grid>
      </AppBar>
      <div className={classes.offset}></div>
    </React.Fragment>
  );
};

export default SiteHeader;
