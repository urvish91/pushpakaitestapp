import {
  CardMedia,
  FormControl,
  Grid,
  MenuItem,
  Select,
  Snackbar,
  Typography,
} from "@material-ui/core";
import {
  createStyles,
  Theme,
  withStyles,
  WithStyles,
} from "@material-ui/core/styles";
import { Pie } from "react-chartjs-2";
import Dialog from "@material-ui/core/Dialog";
import React, { useEffect, useState } from "react";
import CameraGridItem from "./components/CameraGridItem";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";

const styles = (theme: Theme) =>
  createStyles({
    root: {
      margin: 0,
      padding: theme.spacing(2),
    },
    closeButton: {
      position: "absolute",
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.grey[500],
    },
  });

const DialogTitle = withStyles(styles)((props: DialogTitleProps) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme: Theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

export interface DialogTitleProps extends WithStyles<typeof styles> {
  id: string;
  children: React.ReactNode;
  onClose: () => void;
}

interface CameraDataItem {
  imageURL: string;
  title: string;
}

interface SnackItem {
  open: boolean;
  message: string;
}

const Home = () => {
  const [camera, setCamera] = useState<string>("");
  const [dialogOpen, setDialogOpen] = useState<boolean>(false);
  const [snackOpen, setSnackOpen] = useState<SnackItem>({open: false, message: ''});
  const [cameraData, setCameraData] = useState<CameraDataItem[]>([]);

  useEffect(() => {
    const items: CameraDataItem[] = [];
    items.push({
      imageURL:
        "https://images.unsplash.com/photo-1478760329108-5c3ed9d495a0?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=967&q=80",
      title: "Camera 1",
    });
    items.push({
      imageURL:
        "https://images.unsplash.com/32/Mc8kW4x9Q3aRR3RkP5Im_IMG_4417.jpg?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80",
      title: "Camera 2",
    });
    items.push({
      imageURL:
        "https://images.unsplash.com/photo-1506259091721-347e791bab0f?ixid=MnwxMjA3fDB8MHxzZWFyY2h8N3x8YWJzdHJhY3R8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=60",
      title: "Camera 3",
    });
    items.push({
      imageURL:
        "https://images.unsplash.com/flagged/photo-1567400358593-9e6382752ea2?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MjR8fGFic3RyYWN0fGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=60",
      title: "Camera 4",
    });

    setCameraData(items);
  }, []);

  return (
    <Grid container direction="column">
      <Grid item xs={12} container justify="space-between">
        <FormControl>
          <Select
            value={camera}
            displayEmpty
            onChange={(e) => setCamera(e.target.value as string)}
          >
            <MenuItem value="">None</MenuItem>
            <MenuItem value="Option 1">Option 1</MenuItem>
            <MenuItem value="Option 2">Option 2</MenuItem>
            <MenuItem value="Option 3">Option 3</MenuItem>
          </Select>
        </FormControl>

        <FormControl>
          <Select value={camera} displayEmpty>
            <MenuItem value="">None</MenuItem>
            <MenuItem value="Option 1">Option 1</MenuItem>
            <MenuItem value="Option 2">Option 2</MenuItem>
            <MenuItem value="Option 3">Option 3</MenuItem>
          </Select>
        </FormControl>
      </Grid>
      <Grid container>
        {cameraData.map((item, index) => {
          return (
            <Grid item xs={6}>
              <CameraGridItem
              key={index.toString()}
                title={item.title}
                imageURL={item.imageURL}
                onClick={(action: string) => {
                  if(action === 'play') {
                    setDialogOpen(true);
                  } else if(action === 'mic') {
                    setSnackOpen({open: true, message: `Mic Clicked for ${item.title}`})
                  } else if(action === 'full_screen') {
                    setSnackOpen({open: true, message: `Full Screen Clicked for ${item.title}`})
                  }
                }}
              />
            </Grid>
          );
        })}
      </Grid>

      {/** DIALOG WHICH WE ARE GOING TO DISPLAY... */}
      <Dialog
        aria-labelledby="custom-dialog"
        fullWidth={true}
        maxWidth="md"
        open={dialogOpen}
      >
        <DialogTitle id="custom-dialog" onClose={() => setDialogOpen(false)}>
          {""}
        </DialogTitle>
        <DialogContent>
          <Grid container>
            <Grid item xs={8}>
              <CardMedia
                component="video"
                height="80%"
                width="100%"
                autoPlay={true}
                controls={true}
                title="Test Video"
                image="https://www.learningcontainer.com/wp-content/uploads/2020/05/sample-mp4-file.mp4"
              />
            </Grid>
            <Grid item xs={4}>
              <Pie
                type={Pie}
                data={{
                  labels: ["Red", "Blue", "Yellow"],
                  datasets: [
                    {
                      label: "# of Votes",
                      data: [12, 19, 39],
                      backgroundColor: [
                        "rgba(255, 99, 132, 0.2)",
                        "rgba(54, 162, 235, 0.2)",
                        "rgba(255, 206, 86, 0.2)",
                      ],
                      borderColor: [
                        "rgba(255, 99, 132, 1)",
                        "rgba(54, 162, 235, 1)",
                        "rgba(255, 206, 86, 1)",
                      ],
                      borderWidth: 1,
                    },
                  ],
                }}
              />
            </Grid>
          </Grid>
        </DialogContent>
      </Dialog>

      <Snackbar anchorOrigin={{vertical: 'bottom', horizontal:'center'}}
      autoHideDuration={2000}
      message={snackOpen.message}
      open={snackOpen.open}

      onClose={() => setSnackOpen({open: false, message: ''})}
      />
    </Grid>
  );
};

export default Home;
