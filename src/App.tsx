import React, { useEffect, useState } from "react";
import { Divider, Grid, Typography } from "@material-ui/core";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import { deepPurple } from "@material-ui/core/colors";

import HomeIcon from "@material-ui/icons/Home";
import VideocamIcon from "@material-ui/icons/Videocam";
import ListIcon from "@material-ui/icons/List";
import ConfirmationNumberIcon from "@material-ui/icons/ConfirmationNumber";
import NotificationsIcon from "@material-ui/icons/Notifications";

import MenuItem from "./components/MenuItem";
import Home from "./Home";
import SiteHeader from "./components/SiteHeader";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flex: 1,
      padding: 8,
      flexDirection: "column",
    },
    large: {
      width: theme.spacing(7),
      height: theme.spacing(7),
      backgroundColor: deepPurple[500],
      color: theme.palette.getContrastText(deepPurple[500]),
      fontSize: 24,
    },

    divider: {
      margin: theme.spacing(2, 0),
      backgroundColor: "red",
    },
  })
);

interface MenusItem {
  title: string;
  icon: React.ReactNode;
  screenNumber: number;
}

function App() {
  const classes = useStyles();
  const [menus, setMenus] = useState<MenusItem[]>([]);
  const [currentScreen, setCurrentScreen] = useState<number>(0);
  useEffect(() => {
    let items: MenusItem[] = [];
    items.push({
      title: "Live Dashboard",
      icon: <HomeIcon />,
      screenNumber: 0,
    });
    items.push({
      title: "Live Camera",
      icon: <VideocamIcon />,
      screenNumber: 1,
    });
    items.push({ title: "Use Case", icon: <ListIcon />, screenNumber: 2 });
    items.push({
      title: "Custom Dashboard",
      icon: <ConfirmationNumberIcon />,
      screenNumber: 3,
    });
    items.push({
      title: "Screenshot",
      icon: <NotificationsIcon />,
      screenNumber: 4,
    });

    setMenus(items);
  }, []);

  return (
    <div>
      <Grid container className={classes.root}>
        <SiteHeader />

        {/** LEFT PANEL.. */}
        <Grid container justify="space-around">
          <Grid item>
            <Grid container direction="column" style={{ paddingTop: 10 }}>
              {menus.map((item, index) => {
                return (
                  <MenuItem
                    key={index.toString()}
                    icon={item.icon}
                    name={item.title}
                    onPress={() => setCurrentScreen(item.screenNumber)}
                  />
                );
              })}
            </Grid>
          </Grid>
          <Divider
            style={{ marginLeft: 20, marginRight: 20 }}
            orientation="vertical"
            flexItem={true}
          />

          <Grid item style={{ flex: 1, minHeight: "100vh" }}>
          
              {currentScreen === 0 ? (
                <Home />
              ) : currentScreen === 1 ? (
                <Typography>Live Camera Screen Content</Typography>
              ) : currentScreen === 2 ? (
                <Typography>Use case Screen Content</Typography>
              ) : currentScreen === 3 ? (
                <Typography>Custom dashboard Screen Content</Typography>
              ) : currentScreen === 4 ? (
                <Typography>Screenshot Screen Content</Typography>
              ) : null}
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
}

export default App;
